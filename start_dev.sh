#!/bin/bash


# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
    echo "Shutting down dev environment.."
    docker-compose down
}

sudo sysctl -w vm.max_map_count=262144

sudo chown $(id -u):$(id -g) -R .

while true; do sleep 10; sudo chown $(id -u):$(id -g) -R ./target/; done &

docker-compose up -d
docker-compose logs -f api
