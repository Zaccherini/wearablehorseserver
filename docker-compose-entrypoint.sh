#!/bin/bash

#if hash nc 2>/dev/null; then
#    echo "nc already installed"
#else
#    apt-get update && apt-get install -y --no-install-recommends \
#		netcat \
#	&& rm -rf /var/lib/apt/lists/*
#fi
#
#while true
#do
#    rt=$(nc -z -w 1 sqlazure 1433)
#    if [ $? -eq 0 ]; then
#        echo "DB is UP"
#        break
#    fi
#    echo "DB is not yet reachable;sleep for 5s before retry"
#    sleep 5
#done
#while true
#do
#    rt=$(nc -z -w 1 elasticsearch 9200)
#    if [ $? -eq 0 ]; then
#        echo "ES is UP"
#        break
#    fi
#    echo "ES is not yet reachable;sleep for 5s before retry"
#    sleep 5
#done

echo "/············································/"
echo "  Starting backend project"
echo "  · Rest Api on port 8080"
echo "  · Remote Debug on port 5005"
echo "/············································/"

mvn clean spring-boot:run
