#!/bin/bash

# Any subsequent(*) commands which fail will cause the shell script to exit immediately
set -e

####################### INITIAL MIGRATION ##########################
## 1. compilare la regione USER RESULT_SICRA con i dati opportuni
## 2. in db.changelog-1.0.xml si avrà il risultato della migration iniziale
## 3. al termine dell'operazione si potrà lanciare direttamente su wildfly
####################################################################

##### SCRIPT VAR #####
CURRENT_DIR=`pwd`
LIQUIBASE_DIR="$CURRENT_DIR/.."
oldInitialSchema="$LIQUIBASE_DIR/changelog/00000000000000_initial_schema.xml"
TMP_CHANGELOG="$LIQUIBASE_DIR/changelog/db-changelog-new.changelog.xml";

##### PROJECT DIRS #####
PROJ_ROOT=`cd "../../../../../.." && pwd`

# prima compilo
echo "what" ${PROJ_ROOT}
echo "datasource url: " ${SPRING_DATASOURCE_URL}
cd ${PROJ_ROOT} && mvn compile -P dev,add-migration;

# db diff
echo "Running JPA Diff against empty database"
mvn liquibase:diff -P dev,add-migration \
  -Dmigration.db.url="$SPRING_DATASOURCE_URL" \
  -Dmigration.db.user="$SPRING_DATASOURCE_USERNAME" \
  -Dmigration.db.psw="$SPRING_DATASOURCE_PASSWORD"

# setto l'id come initial schema
sed -i -E 's/[0-9]{13}-/0000000000000-/' ${TMP_CHANGELOG}

# aggiusto i datetime che liquibase imposta come BYTEA
sed -i 's/BYTEA/TIMESTAMP WITHOUT TIME ZONE/' ${TMP_CHANGELOG}

# rimuovo autoincrement (problemi con mssql)
sed -i 's/autoIncrement="true" //' ${TMP_CHANGELOG}

# aggiusto hibernate sequence
sed -i '/<createSequence/c\        <createSequence sequenceName="sequence_generator" startValue="1050" incrementBy="50"\/>' ${TMP_CHANGELOG}

# sovrascrivo changelog-1.0
# cp -rf "${oldInitialSchema}" "${oldInitialSchema}.bak"
cp -rf "${TMP_CHANGELOG}" "${oldInitialSchema}"

# ricompilazione con nuovo schema
cd ${PROJ_ROOT} && mvn clean compile;

# cancellazione file temporaneo
echo "Cancellazione del file temporaneo ${TMP_CHANGELOG}"
rm "${TMP_CHANGELOG}"

