#!/bin/sh

CHANGELOG_DESC="$1"

docker-compose -f scripts/migration-compose.yml down -v;
docker-compose -f scripts/migration-compose.yml run --rm api ./docker-add-migration.sh "${CHANGELOG_DESC}"
docker-compose -f scripts/migration-compose.yml down -v;

# Fix permessi
echo "Fix permessi"
sudo chown $(id -u):$(id -g) -R ../../../../../


