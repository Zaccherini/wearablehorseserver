package com.mycompany.myapp.web.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class WearableDeviceDTO {

    private Long id;
    private String name;
    private Long horseId;

}
