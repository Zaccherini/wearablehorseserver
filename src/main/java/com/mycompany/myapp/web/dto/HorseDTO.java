package com.mycompany.myapp.web.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class HorseDTO {
    private Long id;
    private String name;
    private String location;
    private Byte[] image;
    private OwnerDTO owner;
    private WearableDeviceDTO device;
}
