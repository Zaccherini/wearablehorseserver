package com.mycompany.myapp.web.handlers;

import com.mycompany.myapp.service.authentication.EmailAlreadyUsedException;
import com.mycompany.myapp.service.authentication.UsernameAlreadyUsedException;
import com.mycompany.myapp.web.rest.errors.InvalidPasswordException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RegisterExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {UsernameAlreadyUsedException.class, EmailAlreadyUsedException.class, InvalidPasswordException.class})
    protected ResponseEntity<Object> handleBusinessException(RuntimeException exception, WebRequest request) {
        logger.info("DEBUG" + exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
    }
}
