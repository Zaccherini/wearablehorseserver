package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.service.HorseService;
import com.mycompany.myapp.web.dto.HorseDTO;
import com.mycompany.myapp.web.dto.OwnerDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/api")
public class HorseResource {

    private final HorseService horseService;


    public HorseResource(HorseService horseService) {
        this.horseService = horseService;
    }

    @PostMapping("/horse")
    public ResponseEntity<HorseDTO> saveHorseToOwner (@RequestBody HorseDTO horse) {
        HorseDTO result = horseService.saveNewHorse(horse);
        if (result == null) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        return ResponseEntity.ok().body(result);
    }

    // TODO: Internal Server Error (500) if id not present
    @DeleteMapping("/horse/{id}")
    public void deleteById(@PathVariable Long id) {
        horseService.deleteById(id);
    }

    @GetMapping("/horse/{id}")
    public ResponseEntity<HorseDTO> findById(@PathVariable Long id) {
        HorseDTO result = horseService.findById(id);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/horse")
    public ResponseEntity<List<HorseDTO>> findAll() {
        List<HorseDTO> result = horseService.findAll();
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/horses_by_owner")
    public List<HorseDTO> findHorsesByOwner(@RequestBody OwnerDTO owner) {
        return horseService.findHorsesByOwner(owner);
    }

}
