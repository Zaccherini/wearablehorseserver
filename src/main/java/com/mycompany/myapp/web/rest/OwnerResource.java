package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.authentication.User;
import com.mycompany.myapp.service.OwnerService;
import com.mycompany.myapp.service.authentication.UserService;
import com.mycompany.myapp.web.dto.OwnerDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/api")
public class OwnerResource {

    private final OwnerService ownerService;
    private final UserService userService;

    public OwnerResource(OwnerService ownerService, UserService userService) {
        this.ownerService = ownerService;
        this.userService = userService;
    }

    @PostMapping("/owner")
    public ResponseEntity<OwnerDTO> createOwner(@RequestBody OwnerDTO owner) {
        OwnerDTO result = ownerService.createOwner(owner);
        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping("/owner/{id}")
    public void deleteById(@PathVariable Long id) {
        ownerService.deleteById(id);
        log.info("Deleted owner with id {}", id);
    }

    @DeleteMapping("/owner")
    public void deleteAll() {
        ownerService.deleteAll();
        userService.deleteAll();
        log.info("Deleted all owner");
    }

    @PostMapping("/owner/username")
    public ResponseEntity<OwnerDTO> findByUsername(@RequestBody String username) {
        OwnerDTO result = ownerService.findByUsername(username.toLowerCase());
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/owner/{id}")
    public ResponseEntity<OwnerDTO> findById(@PathVariable Long id) {
        OwnerDTO result = ownerService.findById(id);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/owner")
    public ResponseEntity<List<OwnerDTO>> findAll() {
        List<OwnerDTO> result = ownerService.findAll();
        return ResponseEntity.ok().body(result);
    }

}
