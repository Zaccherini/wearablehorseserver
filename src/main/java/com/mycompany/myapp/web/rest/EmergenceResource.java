package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.service.dto.EmergenceMessageDTO;
import com.mycompany.myapp.service.EmergenceService;
import com.mycompany.myapp.service.twilio.SenderTwilio;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(value = "/api")
public class EmergenceResource {

    private final EmergenceService emergenceService;

    public EmergenceResource(EmergenceService emergenceService) {
        this.emergenceService = emergenceService;
    }

    @PostMapping("/emergence")
    public void receiveEmergence(@RequestBody EmergenceMessageDTO message) {
        this.emergenceService.sendEmergence(message);
        log.info("Emergence message sent");
    }

}
