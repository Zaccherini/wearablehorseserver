package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.service.DeviceService;
import com.mycompany.myapp.web.dto.WearableDeviceDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/api")
public class DeviceResource {

    private final DeviceService deviceService;

    public DeviceResource(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @PostMapping("/device")
    public ResponseEntity<WearableDeviceDTO> saveDeviceToHorse (@RequestBody WearableDeviceDTO device) {
        System.out.println(device.toString());
        WearableDeviceDTO result = deviceService.saveDevice(device);
        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping("/device/{id}")
    public void deleteById(@PathVariable Long id) {
        deviceService.deleteById(id);
    }

    @GetMapping("/device/{id}")
    public ResponseEntity<WearableDeviceDTO> findById(@PathVariable Long id) {
        WearableDeviceDTO result = deviceService.findById(id);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/device")
    public ResponseEntity<List<WearableDeviceDTO>> findAll() {
        List<WearableDeviceDTO> result = deviceService.findAll();
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/device/horse/{horseId}")
    public ResponseEntity<WearableDeviceDTO> findByHorse(@PathVariable Long horseId) {
        WearableDeviceDTO result = deviceService.findByHorseId(horseId);
        return ResponseEntity.ok().body(result);
    }
}
