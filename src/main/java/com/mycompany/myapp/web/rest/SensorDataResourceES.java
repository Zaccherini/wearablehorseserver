package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.service.SensorDataService;
import com.mycompany.myapp.service.dto.SensorDataDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api")
public class SensorDataResourceES {

    private final SensorDataService sensorDataService;

    public SensorDataResourceES(SensorDataService sensorDataService) {
        this.sensorDataService = sensorDataService;
    }

    @PostMapping("/sensor-data")
    public ResponseEntity<SensorDataDTO> createSensorData(@RequestBody SensorDataDTO sensorDataDTO){
        SensorDataDTO savedSensorData = sensorDataService.save(sensorDataDTO);
        return ResponseEntity.ok().body(savedSensorData);
    }

    @DeleteMapping(value = "/sensor-data/{id}")
    public void delete(@PathVariable(value = "id") Long id) {
        sensorDataService.deleteById(id);
        log.info("Deleted sensor-data document with id:" + id);
    }

    @DeleteMapping(value = "/sensor-data")
    public void cleanIndex() {
        sensorDataService.deleteAll();
        log.info("Deleted all documents within sensor-data index");
    }

    @GetMapping("/sensor-data/{id}")
    public ResponseEntity<SensorDataDTO> getSensorDataById(@PathVariable Long id) {
        SensorDataDTO result = sensorDataService.findOneById(id);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/sensor-data")
    public ResponseEntity<List<SensorDataDTO>> getSensorData() {
        List<SensorDataDTO> result = sensorDataService.findAll();
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/sensor-data/last")
    public ResponseEntity<SensorDataDTO> getLastSensorData() {
        SensorDataDTO result = sensorDataService.getLastSensorData();
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/sensor-data/device/{id}")
    public ResponseEntity<List<SensorDataDTO>> getSensorDataDeviceId(@PathVariable Long id) {
        List<SensorDataDTO> result = sensorDataService.findAllByDevice(id);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/sensor-data/last/device/{id}")
    public ResponseEntity<SensorDataDTO> getLastSensorData(@PathVariable Long id) {
        SensorDataDTO result = sensorDataService.getLastSensorDataByDevice(id);
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/basic-test")
    public ResponseEntity<String> basicTest() {
        return ResponseEntity.ok().body("ok");
    }
}
