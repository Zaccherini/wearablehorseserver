package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Horse;
import com.mycompany.myapp.domain.Owner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HorseRepository extends JpaRepository<Horse, Long> {

    @Query(value = "SELECT COALESCE(MAX(id), 0) + 1  FROM Horse")
    Long nextId();

    List<Horse> findByOwner(Owner owner);
}
