package com.mycompany.myapp.repository.elasticsearch;

import com.mycompany.myapp.domain.SensorData;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface SensorDataRepository extends ElasticsearchRepository<SensorData, Long> {

    List<SensorData> findFirstByOrderByIdDesc(Pageable pageable);

    List<SensorData> findFirstByOrderByUploadingDateDesc(Pageable pageable);

    List<SensorData> findAllByDeviceId(Long id, Pageable pageable);

    List<SensorData> findFirstByDeviceIdOrderByUploadingDateDesc(Long id, Pageable pageable);
}
