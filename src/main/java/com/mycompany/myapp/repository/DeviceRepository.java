package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.WearableDevice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DeviceRepository extends JpaRepository<WearableDevice, Long> {

    @Query(value = "SELECT COALESCE(MAX(id), 0) + 1  FROM WearableDevice ")
    Long nextId();

    Optional<WearableDevice> findAllById(Long id);

    Optional<WearableDevice> findAllByHorse_Id(Long id);
}
