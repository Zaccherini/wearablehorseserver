package com.mycompany.myapp.repository.authentication;

import com.mycompany.myapp.domain.authentication.Authority;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
