package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Owner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OwnerRepository extends JpaRepository<Owner, Long> {

    @Query(value = "SELECT COALESCE(MAX(id), 0) + 1  FROM Owner")
    Long nextId();

    Optional<Owner> findHorseOwnerByUsername(String username);

}
