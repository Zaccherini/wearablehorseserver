//package com.mycompany.myapp.service.authentication;
//
//import com.mycompany.myapp.domain.authentication.User;
//
//import io.github.jhipster.config.JHipsterProperties;
//
//import java.nio.charset.StandardCharsets;
//import java.util.Locale;
//import java.util.Properties;
//import javax.mail.*;
//import javax.mail.internet.InternetAddress;
//import javax.mail.internet.MimeMessage;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.context.MessageSource;
//import org.springframework.mail.MailException;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.mail.javamail.MimeMessageHelper;
//import org.springframework.scheduling.annotation.Async;
//import org.springframework.stereotype.Service;
//import org.thymeleaf.context.Context;
//import org.thymeleaf.spring5.SpringTemplateEngine;
//
///**
// * Service for sending emails.
// * <p>
// * We use the {@link Async} annotation to send emails asynchronously.
// */
//@Service
//public class MailService {
//
//    private final Logger log = LoggerFactory.getLogger(MailService.class);
//
//    private static final String USER = "user";
//    private static final String FROM_MAIL = "alex.zaccherini1997@gmail.com";
//
//    private static final String BASE_URL = "https://wearable-horse-api-2-xyhngrytsq-uc.a.run.app";
//
//    private final JHipsterProperties jHipsterProperties;
//
//
//    private final MessageSource messageSource;
//
//
//    public MailService(JHipsterProperties jHipsterProperties, MessageSource messageSource,
//                       SpringTemplateEngine templateEngine) {
//
//        this.jHipsterProperties = jHipsterProperties;
//        this.messageSource = messageSource;
//    }
//
//    @Async
//    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
//        log.info("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
//            isMultipart, isHtml, to, subject, content);
//
//        // Prepare message using a Spring helper
//        String host = "smtp.gmail.com";
//        Properties properties = System.getProperties();
//
//        // Setup mail server
//        properties.put("mail.smtp.host", host);
//        properties.put("mail.smtp.port", "465");
//        properties.put("mail.smtp.ssl.enable", "true");
//        properties.put("mail.smtp.auth", "true");
//
//        // Get the Session object.// and pass username and password
//        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
//
//            protected PasswordAuthentication getPasswordAuthentication() {
//
//                return new PasswordAuthentication("alex.zaccherini1997@gmail.com", "zacca4ever");
//
//            }
//
//        });
//
//        session.setDebug(true);
//        try {
//            // Create a default MimeMessage object.
//            MimeMessage message = new MimeMessage(session);
//
//            // Set From: header field of the header.
//            message.setFrom(new InternetAddress(FROM_MAIL));
//
//            // Set To: header field of the header.
//            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
//
//            // Set Subject: header field
//            message.setSubject(subject);
//
//            // Now set the actual message
//            message.setText("FIRST PROPERTIES\n" + content);
//
//            log.info("sending...");
//            // Send message
//            Transport.send(message);
//            log.info("Sent message successfully....");
//        } catch (MessagingException mex) {
//            mex.printStackTrace();
//        }
//    }
//
//    private void sendEmail_2(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
//        log.info("Mail properties 2");
//        final String username = "alex.zaccherini1997@gmail.com";
//        final String password = "zacca4ever";
//
//        Properties prop = new Properties();
//        prop.put("mail.smtp.host", "smtp.gmail.com");
//        prop.put("mail.smtp.port", "587");
//        prop.put("mail.smtp.auth", "true");
//        prop.put("mail.smtp.starttls.enable", "true"); //TLS
//
//        Session session = Session.getInstance(prop,
//            new javax.mail.Authenticator() {
//                protected PasswordAuthentication getPasswordAuthentication() {
//                    return new PasswordAuthentication(username, password);
//                }
//            });
//
//        try {
//
//            Message message = new MimeMessage(session);
//            message.setFrom(new InternetAddress("alex.zaccherini1997@gmail.com"));
//            message.setRecipients(
//                Message.RecipientType.TO,
//                InternetAddress.parse(to)
//            );
//            message.setSubject("Testing Gmail TLS");
//            message.setText("SECOND PROPERTIES\n" + content);
//
//            Transport.send(message);
//
//            System.out.println("Done");
//
//        } catch (MessagingException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Async
//    public void sendEmailFromTemplate(User user, String templateName, String titleKey) {
//        if (user.getEmail() == null) {
//            log.debug("Email doesn't exist for user '{}'", user.getLogin());
//            return;
//        }
//        Locale locale = Locale.forLanguageTag(user.getLangKey());
//        Context context = new Context(locale);
//        context.setVariable(USER, user);
//        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
//        String content = String.format("Attiva il tuo account al seguente link: %s/api/activate?key=%s", BASE_URL, user.getActivationKey());
//        String subject = messageSource.getMessage(titleKey, null, locale);
//        //TODO: ELIMINARE SETAUTHENTICATED DELL'USER
//        sendEmail(user.getEmail(), subject, content, false, true);
//        sendEmail_2(user.getEmail(), subject, content, false, true);
//    }
//
//    @Async
//    public void sendActivationEmail(User user) {
//        log.info("Sending activation email to '{}'", user.getEmail());
//        sendEmailFromTemplate(user, "mail/activationEmail", "email.activation.title");
//    }
//
//    @Async
//    public void sendCreationEmail(User user) {
//        log.debug("Sending creation email to '{}'", user.getEmail());
//        sendEmailFromTemplate(user, "mail/creationEmail", "email.activation.title");
//    }
//
//    @Async
//    public void sendPasswordResetMail(User user) {
//        log.debug("Sending password reset email to '{}'", user.getEmail());
//        sendEmailFromTemplate(user, "mail/passwordResetEmail", "email.reset.title");
//    }
//}
