package com.mycompany.myapp.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class SensorDataDTO implements Serializable {
    private Long id;
    private Long deviceId;

    private int hearthRate;
    private double temperature;
    private double humidity;

    private int horseStatus;

    private double boxTemperature;
    private double boxHumidity;

    private double zAccel;
    private double pitch;
    private double roll;
    private double yaw;

    private Date uploadingDate;
}
