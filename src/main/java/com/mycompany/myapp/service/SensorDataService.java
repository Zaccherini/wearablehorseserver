package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.SensorData;
import com.mycompany.myapp.repository.elasticsearch.SensorDataRepository;
import com.mycompany.myapp.service.dto.SensorDataDTO;
import com.mycompany.myapp.service.mapper.SensorDataMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class SensorDataService {

    private final SensorDataRepository sensorDataRepository;
    private final SensorDataMapper sensorDataMapper;

    public SensorDataService(SensorDataRepository sensorDataRepository, SensorDataMapper sensorDataMapper) {
        this.sensorDataRepository = sensorDataRepository;
        this.sensorDataMapper = sensorDataMapper;
    }

    public SensorDataDTO save(SensorDataDTO dto) {
        if (dto.getId() == null) {
            Long newId = getLastId();
            dto.setId(newId);
        }
        SensorData sensorData = sensorDataMapper.toEntity(dto);
        sensorData.setUploadingDate(new Date());
        SensorData savedData = sensorDataRepository.save(sensorData);

        return sensorDataMapper.toDto(savedData);
    }

    public void deleteById(Long id) {
        sensorDataRepository.deleteById(id);
    }

    public void deleteAll() {
        sensorDataRepository.deleteAll();
    }

    public List<SensorDataDTO> findAll() {
        List<SensorData> data = this.sensorDataRepository.findAll(Pageable.unpaged()).toList();
        for (SensorData dat : data) {
            log.info(dat.getUploadingDate().toString());
        }
        return sensorDataMapper.toDto(data);
    }

    public SensorDataDTO findOneById(Long id) {
        Optional<SensorData> sensorData = this.sensorDataRepository.findById(id);
        return sensorData.map(sensorDataMapper::toDto).orElse(null);
    }

    public SensorDataDTO getLastSensorData() {
        Pageable pageable = PageRequest.of(0, 1);

        List<SensorData> lastDataPage = sensorDataRepository.findFirstByOrderByUploadingDateDesc(pageable);
        SensorData lastData = lastDataPage.get(0);
        return sensorDataMapper.toDto(lastData);
    }

    private Long getLastId() {
        Pageable pageable = PageRequest.of(0, 1);
        List<SensorData> lastIdDataPage;
        try {
            lastIdDataPage = sensorDataRepository.findFirstByOrderByIdDesc(pageable);
        } catch (Exception e) {
            lastIdDataPage = new ArrayList<>();
        }
        if (lastIdDataPage.isEmpty()) {
            log.info("Index empty. NewId = 1");
            return 1L;
        }
        Long newId = lastIdDataPage.get(0).getId() + 1;
        log.info("New id for new SensorData without id: {}", newId);
        return newId;
    }

    //TODO: Inserire controllo su deviceNotFound (index out of bound 0 for length 0
    public List<SensorDataDTO> findAllByDevice(Long id) {
        List<SensorData> data = this.sensorDataRepository.findAllByDeviceId(id, Pageable.unpaged());
        for (SensorData dat : data) {
            log.info(dat.getUploadingDate().toString());
        }
        return sensorDataMapper.toDto(data);
    }

    //TODO: Inserire controllo su deviceNotFound (index out of bound 0 for length 0
    public SensorDataDTO getLastSensorDataByDevice(Long id) {
        Pageable pageable = PageRequest.of(0, 1);

        List<SensorData> lastDataPage = sensorDataRepository.findFirstByDeviceIdOrderByUploadingDateDesc(id, pageable);
        SensorData lastData = lastDataPage.get(0);
        return sensorDataMapper.toDto(lastData);
    }
}
