package com.mycompany.myapp.service.twilio;

//Install the Java helper library from twilio.com/docs/java/install

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import org.springframework.stereotype.Service;

@Service
public class SenderTwilio {
    // Find your Account Sid and Token at twilio.com/console
    // DANGER! This is insecure. See http://twil.io/secure
    public static final String ACCOUNT_SID = "AC4cdd522af853b27661bf7009f4b37e21";
    public static final String AUTH_TOKEN = "1bff3122a8e0f7abc166cd46149c6081";

    public void send(String phone, String msg) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(
            new com.twilio.type.PhoneNumber(phone),
            new com.twilio.type.PhoneNumber("+12074894158"), msg)
                .create();

        System.out.println("Sent message " + msg);
    }
}
