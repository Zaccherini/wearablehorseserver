package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Owner;
import com.mycompany.myapp.repository.OwnerRepository;
import com.mycompany.myapp.service.mapper.OwnerMapper;
import com.mycompany.myapp.web.dto.OwnerDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class OwnerService {

    private final OwnerRepository ownerRepository;
    private final OwnerMapper ownerMapper;

    public OwnerService(OwnerRepository ownerRepository, OwnerMapper ownerMapper) {
        this.ownerRepository = ownerRepository;
        this.ownerMapper = ownerMapper;
    }

    public OwnerDTO createOwner(OwnerDTO dto) {
        if (dto == null || alreadyPresent(dto.getUsername())) {
            log.error("Il dto non esiste o l'username è già stato usato");
            return null;
        }
        if (dto.getId() == null) {
            Long id = ownerRepository.nextId();
            dto.setId(id);
        }

        // gli username sono salvati tutti in minuscolo
        dto.setUsername(dto.getUsername().toLowerCase());
        Owner owner = ownerMapper.toEntity(dto);
        Owner savedOwner = ownerRepository.save(owner);
        return ownerMapper.toDto(savedOwner);
    }

    /**
     * verifica l'unicità dell'username, non necessario per la registrazione in quanto il controllo è già implementato,
     * solo per la creazione diretta
     */
    private boolean alreadyPresent(String username) {
        List<OwnerDTO> dtos = this.findAll();
        return dtos.stream()
            .map(OwnerDTO::getUsername)
            .collect(Collectors.toList())
            .contains(username.toLowerCase());
    }

    public OwnerDTO createRegistrationOwner(String username, String phone) {
        Owner owner = new Owner(username, phone);
        Long id = ownerRepository.nextId();
        owner.setId(id);
        Owner savedOwner = ownerRepository.save(owner);
        return ownerMapper.toDto(savedOwner);
    }

    public void deleteById(Long id) {
        ownerRepository.deleteById(id);
    }

    public void deleteAll() {
        ownerRepository.deleteAll();
    }

    /**
     * Find an owner by username. It is possible because the username is unique.
     */
    public OwnerDTO findByUsername(String username) {
        System.out.println("Search for " + username);
        Optional<Owner> owner = ownerRepository.findHorseOwnerByUsername(username.toLowerCase());
        return owner.map(ownerMapper::toDto).orElse(null);
    }

    public OwnerDTO findById(Long id) {
        Owner owner = ownerRepository.findById(id).orElse(null);
        return ownerMapper.toDto(owner);
    }

    public List<OwnerDTO> findAll() {
        List<Owner> owners = ownerRepository.findAll();
        return ownerMapper.toDto(owners);
    }
}
