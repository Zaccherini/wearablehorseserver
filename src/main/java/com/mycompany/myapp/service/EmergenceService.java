package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.EmergenceMessageDTO;
import com.mycompany.myapp.service.mapper.OwnerMapper;
import com.mycompany.myapp.service.twilio.SenderTwilio;
import com.mycompany.myapp.web.dto.HorseDTO;
import com.mycompany.myapp.web.dto.OwnerDTO;
import com.mycompany.myapp.web.dto.WearableDeviceDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class EmergenceService {

    private static final String emergenceMessage = "ATTENZIONE! C'E' UN'EMERGENZA. IL CAVALLO E' USCITO DAL BOX";

    private final DeviceService deviceService;
    private final HorseService horseService;
    private final OwnerService ownerService;

    private final SenderTwilio senderTwilio;

    public EmergenceService(DeviceService deviceService, HorseService horseService, OwnerService ownerService,
                            OwnerMapper ownerMapper, SenderTwilio senderTwilio) {
        this.deviceService = deviceService;
        this.horseService = horseService;
        this.ownerService = ownerService;
        this.senderTwilio = senderTwilio;
    }

    public void sendEmergence(EmergenceMessageDTO messageDTO) {
        String phoneNumber = getPhoneNumber(messageDTO);
        log.info("Extracted phoneNumber {}", phoneNumber);
//        this.senderTwilio.send(phoneNumber, emergenceMessage);
    }

    private String getPhoneNumber(EmergenceMessageDTO messageDTO) {
        WearableDeviceDTO device = extractDevice(messageDTO);
        if (device == null) {
            log.error("Cannot extract device with id {}", messageDTO.getDeviceId());
            return null;
        }

        HorseDTO horse = horseService.findById(device.getHorseId());
        if (horse == null) {
            log.error("Cannot extract horse with id {}", device.getHorseId());
            return null;
        }

        OwnerDTO owner = horse.getOwner();
        if (owner == null) {
            log.error("Cannot extract owner from horse with id {}", horse.getId());
            return null;
        }

        return owner.getPhone();
    }

    private WearableDeviceDTO extractDevice(EmergenceMessageDTO messageDTO) {
        Long deviceId = messageDTO.getDeviceId();

        return deviceService.findById(deviceId);
    }

}
