package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Horse;
import com.mycompany.myapp.domain.Owner;
import com.mycompany.myapp.repository.HorseRepository;
import com.mycompany.myapp.service.mapper.HorseMapper;
import com.mycompany.myapp.service.mapper.OwnerMapper;
import com.mycompany.myapp.web.dto.HorseDTO;
import com.mycompany.myapp.web.dto.OwnerDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
public class HorseService {

    private final HorseRepository horseRepository;
    private final HorseMapper horseMapper;

    private final OwnerService ownerService;
    private final OwnerMapper ownerMapper;

    public HorseService(HorseMapper horseMapper, HorseRepository horseRepository, OwnerMapper ownerMapper,
                        OwnerService ownerService) {
        this.horseMapper = horseMapper;
        this.horseRepository = horseRepository;
        this.ownerMapper = ownerMapper;
        this.ownerService = ownerService;
    }


    @Transactional
    public HorseDTO saveNewHorse(HorseDTO horse) {
        // check di nullità sull'input
        if (horse == null) {
            return null;
        }

        // aggiorno l'id se non presente
        if (horse.getId() == null) {
            Long id = horseRepository.nextId();
            horse.setId(id);
        }

        // check di presenza dell'owner
        OwnerDTO owner = horse.getOwner();
        if (owner == null || ownerNotPresent(owner.getId())) {
            log.error("Cannot extract owner from horse with id {}", horse.getId());
            return null;
        }

        // fix del nome dell'owner nel caso di modifiche all'user
        horse.getOwner().setUsername(owner.getUsername().toLowerCase());

        // salvataggio della nuova entity Horse
        Horse savedHorse = horseRepository.save(horseMapper.toEntity(horse));
        return horseMapper.toDto(savedHorse);

    }

    public void deleteById(Long id) {
        horseRepository.deleteById(id);
        log.info("Deleted horse with id {}", id);
    }

    @Transactional
    public HorseDTO findById(Long id) {
        Horse horse = horseRepository.findById(id).orElse(null);
        return horseMapper.toDto(horse);
    }

    @Transactional
    public List<HorseDTO> findAll() {
        List<Horse> horses = horseRepository.findAll();
        return horseMapper.toDto(horses);
    }

    @Transactional
    public List<HorseDTO> findHorsesByOwner(OwnerDTO ownerDto) {
        Owner owner = ownerMapper.toEntity(ownerDto);
        List<Horse> horses = horseRepository.findByOwner(owner);
        return horseMapper.toDto(horses);
    }

    private boolean ownerNotPresent(Long ownerId) {
        return ownerService.findById(ownerId) == null;
    }
}
