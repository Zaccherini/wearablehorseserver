package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Horse;
import com.mycompany.myapp.domain.Owner;
import com.mycompany.myapp.service.OwnerService;
import com.mycompany.myapp.web.dto.HorseDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class HorseMapper implements EntityMapper<HorseDTO, Horse> {

    @Override
    public abstract Horse toEntity(HorseDTO dto);

    @Override
    public abstract HorseDTO toDto(Horse dto);

    protected Long map(Owner owner) {
        return owner.getId();
    }
}
