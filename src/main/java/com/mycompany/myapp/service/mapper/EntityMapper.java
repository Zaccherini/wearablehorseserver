package com.mycompany.myapp.service.mapper;

import java.util.List;

/**
 *
 * @param <D> - DTO type parameter
 * @param <E> - ENTITY type parameter
 */
public interface EntityMapper<D, E> {

    E toEntity(D dto);

    D toDto(E entity);

    List<E> toEntity(List<D> dtos);

    List<D> toDto(List<E> entities);

}
