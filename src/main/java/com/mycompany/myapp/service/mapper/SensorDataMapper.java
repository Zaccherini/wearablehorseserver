package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.SensorData;
import com.mycompany.myapp.service.dto.SensorDataDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface SensorDataMapper extends EntityMapper<SensorDataDTO, SensorData> {

    @Override
    SensorData toEntity(SensorDataDTO dto);

}
