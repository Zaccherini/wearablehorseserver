package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Owner;
import com.mycompany.myapp.web.dto.OwnerDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OwnerMapper extends EntityMapper<OwnerDTO, Owner>{

}
