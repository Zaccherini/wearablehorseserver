package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.WearableDevice;
import com.mycompany.myapp.service.HorseService;
import com.mycompany.myapp.web.dto.WearableDeviceDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = { HorseService.class, HorseMapper.class })
public abstract class WearableDeviceMapper implements EntityMapper<WearableDeviceDTO, WearableDevice> {

    @Override
    @Mapping(target = "horse", source = "horseId")
    public abstract WearableDevice toEntity(WearableDeviceDTO dto);

    @Override
    @Mapping(target = "horseId", source = "horse.id")
    public abstract WearableDeviceDTO toDto(WearableDevice entity);

}
