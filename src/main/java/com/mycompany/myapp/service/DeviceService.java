package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.WearableDevice;
import com.mycompany.myapp.repository.DeviceRepository;
import com.mycompany.myapp.service.mapper.WearableDeviceMapper;
import com.mycompany.myapp.web.dto.WearableDeviceDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class DeviceService {

    private final DeviceRepository deviceRepository;
    private final WearableDeviceMapper deviceMapper;

    public DeviceService(DeviceRepository deviceRepository, WearableDeviceMapper deviceMapper) {
        this.deviceRepository = deviceRepository;
        this.deviceMapper = deviceMapper;
    }

    public WearableDeviceDTO saveDevice(WearableDeviceDTO deviceDTO) {
        if (deviceDTO == null) {
            return null;
        }

        if (deviceDTO.getId() == null) {
            Long id = deviceRepository.nextId();
            deviceDTO.setId(id);
        }

        WearableDevice device = deviceRepository.save(deviceMapper.toEntity(deviceDTO));
        return deviceMapper.toDto(device);
    }

    public void deleteById(Long id) {
        deviceRepository.deleteById(id);
    }

    public WearableDeviceDTO findById(Long id) {
        return deviceRepository.findById(id).map(deviceMapper::toDto).orElse(null);
    }

    public List<WearableDeviceDTO> findAll() {
        return deviceMapper.toDto(deviceRepository.findAll());
    }

    public WearableDeviceDTO findByHorseId(Long horseId) {
        Optional<WearableDevice> device = deviceRepository.findAllByHorse_Id(horseId);
        if (!device.isPresent()) {
            log.error("Nessun dispositivo associato al cavallo");
            return null;
        }
        return deviceMapper.toDto(device.get());
    }
}
