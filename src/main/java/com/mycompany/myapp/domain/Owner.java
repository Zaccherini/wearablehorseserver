package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Getter;
import lombok.Setter;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "horse_owner")
@JsonDeserialize
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@Getter
@Setter
public class Owner {

    @Id
    private Long id;

    @JsonProperty("username")
    private String username;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("horses")
    @OneToMany(mappedBy = "owner", cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    private List<Horse> horses = new ArrayList<>();

    public Owner() {
        super();
    }

    public Owner(String username, String phone) {
        this.username = username;
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "HorseOwner{" +
            "id=" + id +
            ", username='" + username + '\'' +
            '}';
    }
}
