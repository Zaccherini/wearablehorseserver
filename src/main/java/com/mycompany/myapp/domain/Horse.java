package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@Table(name = "horse")
public class Horse {

    @Id
    private Long id;

    @NotNull
    @Column(name = "horse_name", nullable = false)
    private String name;

    @Column(name = "horse_location")
    private String location;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "horse_image")
    @Nullable
    private Byte[] image;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name="owner")
    @JsonProperty("owner")
    private Owner owner;

    @OneToOne(mappedBy = "horse", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonProperty("device")
    @Nullable
    private WearableDevice device;

    @Override
    public String toString() {
        return "Horse{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", location='" + location + '\'' +
//            ", owner=" + owner.toString() +
            '}';
    }
}
