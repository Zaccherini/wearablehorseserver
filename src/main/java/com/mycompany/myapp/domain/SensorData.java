package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Getter
@Setter
@Document(indexName = "sensor-data", type = "sensorData", shards = 1)
public class SensorData implements Serializable {

    @Id
    private Long id;
    private Long deviceId;

    private int hearthRate;
    private double temperature;
    private double humidity;

    private int horseStatus;

    private double boxTemperature;
    private double boxHumidity;

    private double zAccel;
    private double pitch;
    private double roll;
    private double yaw;

    private Date uploadingDate;
}
