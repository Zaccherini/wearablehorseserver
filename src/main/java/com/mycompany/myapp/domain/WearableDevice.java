package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "wearable_device")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class WearableDevice {

    @Id
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name="horse")
    @JsonProperty("horse")
    private Horse horse;

}



